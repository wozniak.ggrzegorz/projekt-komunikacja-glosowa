# Głosowa łączność z komputerem
Projekt wykonany w grupie - dotyczy sterowania inteligentnym budynkiem za pomocą mowy.

## Rozpoznawane komendy
* ciepło
* dół
* góra
* jeden
* lewo
* otwórz
* prawo
* przód
* start
* stop
* tył
* wyłącz
* włącz
* zamknij
* zero
* zimno

## W jaki sposób odbywa się sterowanie i za pomocą jakich komend?
Zastanawiano się nad wieloma rozwiązaniami, które można zaliczyc do Smart Home / Smart Building Solutions. Pod uwagę były brane m.in. ochrona przed włamaniami, monitorowanie zużycia mediów, alarmowanie np. o przeciekających rurach czy autouzupełnianie / autozamawianie produktów domowych. Stwierdzono jednak, iż kierunkiem w którym warto podążać będzie zwiększenia komfortu we własnym domu (w sypialni/salonie) 

Komendy obejmują sterowanie:

* Oświetleniem: `włącz`,`stop` - włącza lub wyłącza światło

* Temperaturą: `ciepło`,`zimno` - zmniejsza lub zwiększa ogrzewanie

* Budzikiem: `start`, `stop` - nastawia lub wyłącza budzik

* Oknem: `otwórz`, `zamknij` - uchyla lub zamyka okno

* Roletami: `góra`, `dół` - unosi lub obniża rolety

* Telewizorem: `jeden`, `zero` - włącza lub wyłącza telewizor; `prawo`, `lewo` - obraca ekran telewizora w prawo/lewo

* Fotelem: `przód`, `tył` - pochyla lub odchyla oparcie fotela do przodu

## Typ danych
Pliki dźwiękowe (komendy) mają częstotliwość 8kHz i są zapisywane z rozszerzeniem .wav 

## Etapy projektu
1. `createMFCCdata.py` - tworzy plik `results.csv`, który dla dla każdego z nagranych plików audio przechowuje typ komendy oraz cechy MFCC wyznaczne na podstawie biblioteki open-source https://github.com/jameslyons/python_speech_features/

2. `createSVM.py` - tworzy `svm.sav`, model SVM na podstawie danych w `results.csv`

3. `classifyTestData.py` - klasyfikuje dane testowe na podstawie modelu SVM

## Wyniki
Po wytrenowaniu SVM na podstawie zbioru komend sprawdzono jego działanie na nowych plikach audio (każdy z nas nagrał po jednym pliku dla każdej z 16 komend).
Komendy głosowe zostały poprawnie rozpoznane ze skutecznością 50%.

## Warunki uruchomienia projektu
* Python 3
* biblioteki `pandas`, `numpy`, `scipy`, `pickle`, `sklearn`
* środowisko do uruchamiania kodu (np. PyCharm)

1. Pobierz zawartość repozytorium 
2. Uruchom `classifyTestData.py` w celu obejrzenia wyników klasyfikacji komend testowych

## Członkowie zespołu
1. Grzegorz Woźniak
2. Konrad Wojdon
3. Kinga Pijanowska
4. Olaf Tomaszewski
5. Bartłomiej Tyka
6. Przemysław Żmuda
7. Barbara Chojnowska
